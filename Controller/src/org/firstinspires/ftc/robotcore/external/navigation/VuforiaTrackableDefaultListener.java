package org.firstinspires.ftc.robotcore.external.navigation;

import androidx.annotation.Nullable;

import com.vuforia.TrackableResult;

import org.firstinspires.ftc.robotcore.external.hardware.camera.Camera;
import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;

import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.DEGREES;
import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.RADIANS;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.XYZ;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.YZX;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesReference.EXTRINSIC;

public class VuforiaTrackableDefaultListener implements VuforiaTrackable.Listener {
    private final VuforiaTrackable trackable;
    OpenGLMatrix cameraLocationOnRobot;
    static OpenGLMatrix robotLocation;

    public VuforiaTrackableDefaultListener(VuforiaTrackable trackable) {
        this.trackable = trackable;
    }

    // FIXME: by adding an update() function to this and calling it from the virtual robot's
    // updateAndSensors() method, it should be possible to have these functions actually return
    // valid poses and robot locations.

    /**
     * <p>Returns the {@link OpenGLMatrix} transform that represents the location of the robot
     * on in the FTC Field Coordinate System, or null if that cannot be computed. The returned
     * transformation will map coordinates in the Robot Coordinate System to coordinates in the
     * FTC Field Coordinate System.</p>
     */
    public OpenGLMatrix getRobotLocation() {
        return robotLocation;
    }

    // virtual robot controller will call this to set x, y & heading in field coordinates
    public static void updateRobotLocation(float x, float y, float headingRadians) {
        // System.out.printf("Got x, y, heading: %f %f %f\n", x, y, headingRadians * 180.0/Math.PI);

        robotLocation = OpenGLMatrix
                    .translation(x, y, 0)
                    .multiplied(Orientation.getRotationMatrix(EXTRINSIC, YZX, RADIANS, 0, headingRadians, 0));

        // System.out.printf("Robot Translation: %s\n", robotLocation.getTranslation());
        //Orientation rotation = Orientation.getOrientation(robotLocation, EXTRINSIC, XYZ, DEGREES);
        // System.out.printf("Robot Heading: {Roll, Pitch, Heading} = %.0f, %.0f, %.0f\n", rotation.firstAngle, rotation.secondAngle, rotation.thirdAngle);
    }

    /**
     * Returns the pose of the trackable if it is currently visible. If it is not currently
     * visible, null is returned. The pose of the trackable is the location of the trackable
     * in the FTC Camera Coordinate System.
     *
     */
    public @Nullable OpenGLMatrix getFtcCameraFromTarget()
    {
        //FIXME: or is this the other way around?
        return getRobotLocation().multiplied(this.cameraLocationOnRobot);
    }

    public OpenGLMatrix getPose() {
        // System.out.println("getPose() is deprecated, use getFtcCameraFromTarget() instead");
        return getFtcCameraFromTarget();
    }


    public boolean isVisible() {
        // FIXME: could do a better job here and calculate if we should be able to see the target based on assumptions
        // about the field of view and distance of the robot from the target
        // maybe assume a 90deg field of view and a 3-4 foot distance?
        return true;
    }

    public void setPhoneInformation(OpenGLMatrix phoneLocationOnRobot, VuforiaLocalizer.CameraDirection cameraDir) {
        cameraLocationOnRobot = phoneLocationOnRobot;
    }
    public void setCameraLocationOnRobot(CameraName cameraName, OpenGLMatrix cameraLocationOnRobot) {
        this.cameraLocationOnRobot = cameraLocationOnRobot;
    }

    @Override
    public void onTracked(TrackableResult trackableResult, CameraName cameraName, @Nullable Camera camera, @Nullable VuforiaTrackable child) {
    }

    @Override
    public void onNotTracked() {
    }

    @Override
    public void addTrackable(VuforiaTrackable trackable) {
    }
}
