/*
Copyright (c) 2016 Robert Atkinson

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted (subject to the limitations in the disclaimer below) provided that
the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

Neither the name of Robert Atkinson nor the names of his contributors may be used to
endorse or promote products derived from this software without specific prior
written permission.

NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package org.firstinspires.ftc.robotcore.external.navigation;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.sun.istack.internal.NotNull;
import com.vuforia.Image;
import com.vuforia.TrackableResult;


import org.firstinspires.ftc.robotcore.external.hardware.camera.Camera;
import org.firstinspires.ftc.robotcore.external.hardware.camera.CameraName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;




public class VuforiaLocalizer {
    FrameQueue frameQueue = new FrameQueue();

    private class VuforiaTrackablesImpl extends ArrayList<VuforiaTrackable> implements VuforiaTrackables {
        VuforiaLocalizer localizer;
        String name;
        public void setName(String name) { this.name = name; }
        public String getName() { return this.name; }
        public void activate() {}
        public void deactivate() {}

        VuforiaTrackablesImpl(VuforiaLocalizer localizer) {
            this.localizer = localizer;
        }

        public VuforiaLocalizer getLocalizer() {
            return localizer;
        }
    }

    private class VuforiaTrackableImpl implements VuforiaTrackable {

        private OpenGLMatrix ftcFieldFromTarget;
        private Listener listener;
        private OpenGLMatrix location;
        private Object userData;
        private String name;
        private VuforiaTrackables trackables;

        public VuforiaTrackableImpl(VuforiaTrackablesImpl trackables) {
            this.trackables = trackables;
            this.listener = new VuforiaTrackableDefaultListener(this);
        }

        @Override
        public void setListener(@Nullable Listener listener) {
            this.listener = listener;
        }

        @Override
        public Listener getListener() {
            return listener;
        }

        @Override
        public void setLocationFtcFieldFromTarget(@NonNull OpenGLMatrix ftcFieldFromTarget) {
            this.ftcFieldFromTarget = ftcFieldFromTarget;
        }

        @Override
        public void setLocation(@NonNull OpenGLMatrix location) {
            this.location = location;
        }

        @NonNull
        @Override
        public OpenGLMatrix getFtcFieldFromTarget() {
           return ftcFieldFromTarget;
        }

        @NonNull
        @Override
        public OpenGLMatrix getLocation() {
            return location;
        }

        @Override
        public void setUserData(Object object) {
            this.userData = object;

        }

        @Override
        public Object getUserData() {
            return userData;
        }

        @Override
        public VuforiaTrackables getTrackables() {
            return trackables;
        }

        @Override
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public VuforiaTrackable getParent() {
            return null;
        }
    }

    public VuforiaTrackables loadTrackablesFromAsset(String trackablesFile) {
        VuforiaTrackablesImpl trackables = new VuforiaTrackablesImpl(this);

        //FIXME: add field locations? names?
        trackables.add(new VuforiaTrackableImpl(trackables));
        trackables.add(new VuforiaTrackableImpl(trackables));
        trackables.add(new VuforiaTrackableImpl(trackables));
        trackables.add(new VuforiaTrackableImpl(trackables));
        return trackables;
    }

    public class CloseableFrame {
        public int getNumImages() {
            return 1;
        }

        public Image getImage(int i) {
            return new Image();
        }

        public void close() {
        }
    };

    public class FrameQueue {
        CloseableFrame frame = new CloseableFrame();

        public CloseableFrame take() throws InterruptedException {
            return frame;
        }
    };

    public FrameQueue getFrameQueue() {
        return frameQueue;
    };

    public void setFrameQueueCapacity(int queueCapacity) {
    }


    public enum CameraDirection {
        FRONT,
        BACK
    };

    public static class Parameters {

        public String vuforiaLicenseKey;
        public CameraDirection cameraDirection;
        public Parameters.CameraMonitorFeedback cameraMonitorFeedback;
        public Boolean useExtendedTracking;
        public CameraName cameraName;


        public Parameters(int cameraViewId) {
        }

        public Parameters() {
            
        }

        public enum CameraMonitorFeedback {
            AXES
        }
    };
}
