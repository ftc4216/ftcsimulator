package com.qualcomm.robotcore.hardware;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

public class OpticalDistanceSensorImpl implements OpticalDistanceSensor{
    DistanceSensor sensor;

    public OpticalDistanceSensorImpl(DistanceSensor sensor) {
        this.sensor = sensor;
    }


    @Override
    public double getLightDetected() {
        return sensor.getDistance(DistanceUnit.MM);
    }


    //NOTE: with this virtual sensor, can use getRawLightDetected() getRawLightDetectedMax() to make sure sensor is showing valid value
    @Override
    public double getRawLightDetected() {
        return sensor.getDistance(DistanceUnit.MM);
    }

    @Override
    public double getRawLightDetectedMax() {
        return Float.MAX_VALUE;
    }

    @Override
    public void enableLed(boolean enable) {
    }

    @Override
    public String status() {
        return "Virtual optical distance sensor connected";
    }

    @Override
    public Object updateState(double milliseconds, Object doubleDistance) {
        return null;
    }
}
