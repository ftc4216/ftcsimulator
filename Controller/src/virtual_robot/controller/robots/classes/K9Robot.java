package virtual_robot.controller.robots.classes;

import com.qualcomm.hardware.bosch.BNO055IMUImpl;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cColorSensor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorExImpl;
import com.qualcomm.robotcore.hardware.GyroSensorImpl;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensorImpl;
import com.qualcomm.robotcore.hardware.ServoImpl;
import com.qualcomm.robotcore.hardware.configuration.MotorType;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;

import javafx.fxml.FXML;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import virtual_robot.controller.BotConfig;
import virtual_robot.controller.VirtualBot;
import virtual_robot.controller.VirtualRobotController;
import virtual_robot.util.AngleUtils;

/**
 * For internal use only. Represents a robot with two standard wheels, color sensor, four distance sensors,
 * a Gyro Sensor, and a Servo-controlled arm on the back.
 *
 * TwoWheelBot is the controller class for the "two_wheel_bot.fxml" markup file.
 */
@BotConfig(name = "K9 Robot", filename = "k9_robot", disabled=false)
public class K9Robot extends VirtualBot {

    private final MotorType MOTOR_TYPE = MotorType.Neverest40;
    private DcMotorExImpl leftMotor = null;
    private DcMotorExImpl rightMotor = null;
    private BNO055IMUImpl imu = null;
    private ModernRoboticsI2cColorSensor colorSensor = null;
    private OpticalDistanceSensorImpl distanceSensor = null;
    private VirtualRobotController.DistanceSensorImpl internalDistanceSensor;


    private ServoImpl servo_1 = null;
    private ServoImpl servo_6 = null;


    //The backServoArm object is instantiated during loading via a fx:id property.
   // @FXML Rectangle servo1Arm;
   // @FXML Rectangle servo6Arm;

    private double wheelCircumference;
    private double interWheelDistance;



    public K9Robot(){
        super();
    }

    public void initialize(){
        super.initialize();

        hardwareMap.setActive(true);
        leftMotor = (DcMotorExImpl)hardwareMap.get(DcMotorEx.class, "motor_1");
        rightMotor = (DcMotorExImpl)hardwareMap.get(DcMotorEx.class, "motor_2");
        imu = hardwareMap.get(BNO055IMUImpl.class, "imu");
        colorSensor = (ModernRoboticsI2cColorSensor) hardwareMap.colorSensor.get("colorSensor");

        distanceSensor = (OpticalDistanceSensorImpl) hardwareMap.opticalDistanceSensor.get("light_sensor");
        servo_1 = (ServoImpl)hardwareMap.servo.get("servo_1");
        servo_6 = (ServoImpl)hardwareMap.servo.get("servo_6");

        wheelCircumference = Math.PI * botWidth / 4.5;
        interWheelDistance = botWidth * 8.0 / 9.0;
        hardwareMap.setActive(false);

        // servo1Arm.getTransforms().add(new Rotate(0, 37.5, 67.5));
    }

    protected void createHardwareMap(){
        hardwareMap = new HardwareMap();
        hardwareMap.put("motor_1", new DcMotorExImpl(MOTOR_TYPE));
        hardwareMap.put("motor_2", new DcMotorExImpl(MOTOR_TYPE));
        hardwareMap.put("imu", new BNO055IMUImpl(this, 10));
        hardwareMap.put("servo_1", new ServoImpl());
        hardwareMap.put("servo_6", new ServoImpl());

        ModernRoboticsI2cColorSensor sensor = new ModernRoboticsI2cColorSensor(controller.new ColorSensorImpl());
        hardwareMap.put("colorSensor", sensor);

        internalDistanceSensor = controller.new DistanceSensorImpl();
        hardwareMap.opticalDistanceSensor.put("light_sensor", new OpticalDistanceSensorImpl(internalDistanceSensor));
    }

    public synchronized void updateStateAndSensors(double millis){

        double deltaLeftPos = leftMotor.update(millis);
        double deltaRightPos = rightMotor.update(millis);
        double leftWheelDist = -deltaLeftPos * wheelCircumference / MOTOR_TYPE.TICKS_PER_ROTATION;
        double rightWheelDist = deltaRightPos * wheelCircumference / MOTOR_TYPE.TICKS_PER_ROTATION;
        double distTraveled = (leftWheelDist + rightWheelDist) / 2.0;
        double headingChange = (rightWheelDist - leftWheelDist) / interWheelDistance;
        double deltaRobotX = -distTraveled * Math.sin(headingRadians + headingChange / 2.0);
        double deltaRobotY = distTraveled * Math.cos(headingRadians + headingChange / 2.0);

        x += deltaRobotX;
        y += deltaRobotY;
        headingRadians += headingChange;

        if (headingRadians > Math.PI) headingRadians -= 2.0 * Math.PI;
        else if (headingRadians < -Math.PI) headingRadians += 2.0 * Math.PI;

        constrainToBoundaries();

        double sensorHeading = AngleUtils.normalizeRadians(headingRadians);
        imu.updateState(millis, headingRadians);
        colorSensor.updateColor(x, y);
        internalDistanceSensor.updateDistance( x - halfBotWidth * Math.sin(sensorHeading),
                y + halfBotWidth * Math.cos(sensorHeading), sensorHeading);



        // System.out.printf("x: %f y: %f", x, y);
        // System.out.printf("heading: %f dist: %f\n", sensorHeading, internalDistanceSensor.getDistance(DistanceUnit.MM));


        VuforiaTrackableDefaultListener.updateRobotLocation((float)(x*25.4*18.0/botWidth), (float)(y*25.4*18.0/botWidth), (float)headingRadians);


        final double piOver2 = Math.PI / 2.0;
    }

    public synchronized void updateDisplay(){
        super.updateDisplay();
//        ((Rotate) servo1Arm.getTransforms().get(0)).setAngle(-180.0 * servo_1.getInternalPosition());
//        ((Rotate) servo6Arm.getTransforms().get(0)).setAngle(-180.0 * servo_6.getInternalPosition());
    }

    public void powerDownAndReset(){
        leftMotor.stopAndReset();
        rightMotor.stopAndReset();
    }


}
